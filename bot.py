#!/usr/bin/env python3

import argparse
import functools
import json
import logging
import os
import os.path
import sys
import typing

import mistune  # type: ignore
import gitlab as gitlab_module
from bleach import linkifier, sanitizer
from dateutil import parser as dateutil_parser
from gitlab import const as gitlab_const, exceptions as gitlab_exceptions
from gitlab.v4 import objects as gitlab_objects

logger = logging.getLogger(__name__)

DATASETS_GROUP_ID = 6655953
REQUIRED_TAGS = {'dataset', 'datagit'}
MAX_RETRIES = 15

# TODO: Convert h1 and h2 headings to h3. See: https://github.com/mozilla/bleach/issues/508
#       We could allow h1 and h2 tags and then have a filter which converts them to h3.
# TODO: Remove <script> and <style> tags with contents.
#       See: https://github.com/jvanasco/bleach_extras
cleaner = sanitizer.Cleaner(
    tags=sanitizer.ALLOWED_TAGS + ['p', 'br', 'h3', 'h4', 'h5', 'pre'],
    strip=True,
    filters=[functools.partial(linkifier.LinkifyFilter, callbacks=linkifier.DEFAULT_CALLBACKS)],
)

markdown = mistune.Markdown(escape=False, hard_wrap=True)


def environ_or_required(key: str) -> typing.Dict[str, typing.Any]:
    if os.environ.get(key, None):
        return {'default': os.environ[key]}
    else:
        return {'required': True}


def gitlab_project(gitlab: gitlab_module.Gitlab, project: typing.Dict) -> typing.Dict:
    gitlab_project = gitlab.projects.get(project['id'], lazy=True)
    res_project = gitlab_project.additionalstatistics.get(max_retries=MAX_RETRIES, retry_transient_errors=True)
    assert res_project is not None
    statistics = res_project.attributes
    res_issues = gitlab_project.issues_statistics.get(max_retries=MAX_RETRIES, retry_transient_errors=True)
    assert res_issues is not None
    issues_statistics = res_issues.attributes

    return {
        'id': str(project['id']),
        'is_active': not project['archived'],
        'name': project['name'],
        'description': project['description'],
        'web_url': project['web_url'],
        'ssh_url_to_repo': project['ssh_url_to_repo'],
        'http_url_to_repo': project['http_url_to_repo'],
        'gitlab': {
            'commit_count': project['statistics']['commit_count'],
            'storage_size': project['statistics']['storage_size'],
            'fetches_30days_count': statistics['fetches']['total'],
            'opened_issues_count': issues_statistics['statistics']['counts']['opened'],
            'closed_issues_count': issues_statistics['statistics']['counts']['closed'],
            'all_issues_count': issues_statistics['statistics']['counts']['all'],
            'avatar_url': project['avatar_url'],
            'created_at': project['created_at'],
            'created_at_timestamp': dateutil_parser.isoparse(project['created_at']).timestamp(),
            'last_activity_at': project['last_activity_at'],
            'last_activity_at_timestamp': dateutil_parser.isoparse(project['last_activity_at']).timestamp(),
            'star_count': project['star_count'],
        },
        'tags': [tag for tag in project['tag_list'] if tag not in REQUIRED_TAGS],
    }


def openml_project(gitlab: gitlab_module.Gitlab, project: typing.Dict) -> typing.Dict:
    result: typing.Dict[str, str] = {}

    if 'openml' not in project['tag_list']:
        return result

    gitlab_project = gitlab.projects.get(project['id'], lazy=True)

    try:
        metadata_json = gitlab_project.files.raw(file_path='dataset/metadata.json', ref='master', max_retries=MAX_RETRIES, retry_transient_errors=True)
    except gitlab_exceptions.GitlabGetError as error:
        if error.response_code == 404:
            logger.warning("Project %(project_id)s is missing the OpenML metadata file.", {'project_id': project['id']})
            return result
        else:
            raise error

    assert metadata_json is not None

    try:
        metadata = json.loads(metadata_json)
    except ValueError:
        logger.warning("Project %(project_id)s OpenML metadata file is not valid JSON.", {'project_id': project['id']}, exc_info=True)
        return result

    if 'data_set_description' not in metadata:
        return result

    if metadata['data_set_description'].get('name', None):
        result['name'] = metadata['data_set_description']['name']

    if metadata['data_set_description'].get('description', None):
        try:
            result['description'] = markdown(f"""OpenML dataset https://www.openml.org/d/{metadata['data_set_description']['id']}\n\n""" + metadata['data_set_description']['description'])
        except Exception:
            logger.warning("Project %(project_id)s failed rendering OpenML markdown description.", {'project_id': project['id']}, exc_info=True)
            return result

    return result


def main(argv: typing.Sequence):
    parser = argparse.ArgumentParser(prog='bot', description="Generates search index.")
    parser.add_argument(
        '-t', '--token',
        help="token to use to connect to GitLab.com, has priority over BOT_TOKEN environment variable, required if the environment variable is not set",
        **environ_or_required('BOT_TOKEN'),
    )
    parser.add_argument(
        '-d', '--debug', action='store_true', default=bool(os.environ.get('BOT_DEBUG', None)),
        help="set logging level to debug, can leak sensitive data, has priority over BOT_DEBUG environment variable",
    )
    parser.add_argument(
        '--output-ids', metavar='FILE', type=argparse.FileType('w', encoding='utf8'), action='store',
        help="only output to a file IDs of GitLab projects to index, use \"-\" for stdout",
    )
    parser.add_argument(
        '--input-ids', metavar='FILE', type=argparse.FileType('r', encoding='utf8'), action='store',
        help="a file of available GitLab project IDs, use \"-\" for stdin",
    )
    parser.add_argument(
        '--output-documents', metavar='FILE', type=argparse.FileType('w', encoding='utf8'), action='store',
        help="output a JSON with search index documents, use \"-\" for stdout",
    )
    parser.add_argument(
        '--input-documents', metavar='FILE', type=argparse.FileType('r', encoding='utf8'), action='store', nargs='+',
        help="a JSON file with search index documents, use \"-\" for stdin",
    )
    parser.add_argument(
        '--parallel-index', metavar='I', type=int, action='store',
        help="index of a job when running multiple jobs in parallel"
    )
    parser.add_argument(
        '--parallel-total', metavar='N', type=int, action='store',
        help="total number of jobs when running multiple jobs in parallel"
    )
    parser.add_argument(
        'project_ids', metavar='ID', type=int, nargs='*',
        help="GitLab project IDs to limit indexing to",
    )

    arguments = parser.parse_args(argv[1:])

    if (arguments.parallel_index is not None and arguments.parallel_total is None) or (arguments.parallel_index is None and arguments.parallel_total is not None):
        parser.error("both --parallel-index and --parallel-total are required, or none")

    if arguments.output_ids is not None and arguments.output_documents is not None:
        parser.error("both --output-ids and --output-documents cannot be used at the same time")

    if arguments.input_documents is not None and arguments.project_ids:
        parser.error("both --input-documents and GitLab project IDs cannot be used at the same time")

    if arguments.input_documents is not None and arguments.input_ids is not None:
        parser.error("both --input-documents and --input-ids cannot be used at the same time")

    if arguments.input_documents is not None and arguments.output_ids is not None:
        parser.error("both --input-documents and --output-ids cannot be used at the same time")

    if arguments.input_ids is not None and arguments.project_ids:
        parser.error("both --input-ids and GitLab project IDs cannot be used at the same time")

    logging.basicConfig(level=logging.DEBUG if arguments.debug else logging.INFO)

    if arguments.input_documents is not None:
        projects_to_index: typing.List[typing.Dict] = []
        for input_json_file in arguments.input_documents:
            projects_to_index.extend(json.load(input_json_file))

        projects_to_index.sort(key=lambda project: int(project['id']), reverse=True)

    else:
        gitlab = gitlab_module.Gitlab('https://gitlab.com', private_token=arguments.token)

        if arguments.input_ids is not None:
            projects: typing.Sequence[gitlab_objects.Project] = [gitlab.projects.get(line.strip(), lazy=True) for line in arguments.input_ids]

        elif arguments.project_ids:
            projects = [gitlab.projects.get(project_id, lazy=True) for project_id in arguments.project_ids]
        else:
            group = gitlab.groups.get(DATASETS_GROUP_ID, lazy=True)

            # TODO: Use keyset pagination. See: https://github.com/python-gitlab/python-gitlab/issues/961
            projects = typing.cast(typing.Sequence[gitlab_objects.Project], group.projects.list(
                visibility=gitlab_const.VISIBILITY_PUBLIC,
                simple=False,
                with_shared=True,
                include_subgroups=True,
                statistics=True,
                as_list=True,
                all=True,
                per_page=100,
                max_retries=MAX_RETRIES,
                retry_transient_errors=True,
            ))

        logger.info("There are %(projects_count)s projects.", {'projects_count': len(projects)})

        if arguments.parallel_total:
            projects = projects[(arguments.parallel_index - 1)::arguments.parallel_total]
            logger.info("Running in parallel (%(parallel_index)s/%(parallel_total)s), indexing %(projects)s.", {
                'projects': len(projects),
                'parallel_index': arguments.parallel_index,
                'parallel_total': arguments.parallel_total,
            })

        if arguments.output_ids is not None:
            for project in projects:
                arguments.output_ids.write(f'{project.id}\n')
            return

        has_errored = False

        projects_to_index = []
        for project in projects:
            try:
                if not hasattr(project, 'statistics'):
                    # If not all data is available. Can be because of a bug in GitLab
                    # (see: https://gitlab.com/gitlab-org/gitlab/issues/38544) or because
                    # we have just project ID.
                    try:
                        project = gitlab.projects.get(project.id, simple=False, statistics=True, max_retries=MAX_RETRIES, retry_transient_errors=True)
                    except gitlab_module.GitlabGetError as error:
                        if error.response_code == 404:
                            # Maybe project was deleted between enumeration time and now.
                            logger.warning("Project %(project_id)s cannot be found. Skipping.", {'project_id': project.id})
                            continue
                        else:
                            raise error

                input_document = project.attributes

                project_access = input_document['permissions']['project_access'] or {}
                group_access = input_document['permissions']['group_access'] or {}
                if project_access.get('access_level', 0) < 40 and group_access.get('access_level', 0) < 40:
                    logger.warning("Project %(project_id)s does not grant necessary permissions to the bot. Skipping.", {'project_id': project.id})
                    continue

                if REQUIRED_TAGS - set(input_document['tag_list']):
                    logger.warning("Project %(project_id)s is missing required tags %(required_tags)s. Skipping.", {'project_id': project.id, 'required_tags': sorted(REQUIRED_TAGS)})
                    continue

                assert 'statistics' in input_document

                index_document = {}
                for transform in [gitlab_project, openml_project]:
                    index_document.update(transform(gitlab, input_document))

                # Sanitize the description. Description can contain a subset of HTML.
                index_document['description'] = cleaner.clean(index_document['description'])

                projects_to_index.append(index_document)
            except:
                has_errored = True
                logger.exception("Exception while preparing index document for project %(project_id)s.", {'project_id': project.id})

        if has_errored:
            sys.exit(1)

    if arguments.output_documents is not None:
        # We indent and sort so that diff-ing is easier.
        json.dump(projects_to_index, arguments.output_documents, ensure_ascii=False, allow_nan=False, indent=2, sort_keys=True)


if __name__ == '__main__':
    main(sys.argv)
