#!/usr/bin/env python3

import unittest

import bot


class Tests(unittest.TestCase):
    def test_cleaner(self):
        self.assertEqual(bot.cleaner.clean("""<a href="<"></a>"""), """<a href="&lt;" rel="nofollow"></a>""")
        self.assertEqual(bot.cleaner.clean("""<a href=">"></a>"""), """<a href=">" rel="nofollow"></a>""")
        self.assertEqual(bot.cleaner.clean("""<?PITarget PIContent?>Test"""), """Test""")
        # Regex to remove HTML tags expects no newlines in tags.
        self.assertEqual(bot.cleaner.clean("""<p\n>Test</p>"""), """<p>Test</p>""")
        # Regex to remove HTML tags expects no spaces around "=" in tag's attributes.
        self.assertEqual(bot.cleaner.clean("""<a href = ">" >Test</a>"""), """<a href=">" rel="nofollow">Test</a>""")

        # TODO: We prefer contents of script tags to be removed.
        #self.assertEqual(bot.cleaner.clean("""<script>alert()</script>Test"""), """Test""")
        # TODO: We prefer <h1> and <h2> to be replaced with <h3>.
        #self.assertEqual(bot.cleaner.clean("""<h1>Test</h1>"""), """<h3>Test</h3>""")


if __name__ == '__main__':
    unittest.main()
